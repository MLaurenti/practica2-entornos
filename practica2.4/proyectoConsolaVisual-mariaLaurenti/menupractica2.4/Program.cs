﻿using System;

namespace menupractica2._4
{
    class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Introduce una opcion: \n1)Maximo entre 2 numeros \n2)Minimo entre 2 numeros \n3)Averiguar si un numero es primo \n4)Suma de dos numeros");
            String opcion = Console.ReadLine();
            int parseo = int.Parse(opcion);
            String numero1;
            String numero2;
           
            
            System.Console.ReadLine();
            switch (parseo)
            {
                case 1:
                    Console.WriteLine("Introduce un numero");
                    numero1 = Console.ReadLine();
                    int parseonumero1 = int.Parse(numero1);
                    Console.WriteLine("Introduce un numero");
                    numero2 = Console.ReadLine();
                    int parseonumero2 = int.Parse(numero2);
                    Console.WriteLine("El numero maximo es :" + Maximo(parseonumero1, parseonumero2));
                    break;
                case 2:
                    Console.WriteLine("Introduce un numero");
                    numero1 = Console.ReadLine();
                    parseonumero1 = int.Parse(numero1);
                    Console.WriteLine("Introduce un numero");
                    numero2 = Console.ReadLine();
                    parseonumero2 = int.Parse(numero2);
                    Console.WriteLine("El numero minimo es :" + Minimo(parseonumero1, parseonumero2));
                    break;
                case 3:
                    Console.WriteLine("Introduce un numero");
                    numero1 = Console.ReadLine();
                    parseonumero1 = int.Parse(numero1);
                    Console.WriteLine(EsPrimo(parseonumero1, true));
                    break;
                case 4:
                    Console.WriteLine("Introduce un numero");
                    numero1 = Console.ReadLine();
                    parseonumero1 = int.Parse(numero1);
                    Console.WriteLine("Introduce un numero");
                    numero2 = Console.ReadLine();
                    parseonumero2 = int.Parse(numero2);
                    Console.WriteLine("El resultado es " + Suma(parseonumero1, parseonumero2));
                    break;
            }
            Console.WriteLine("Pulse una tecla para continuar");
            Console.ReadKey();
    }
 
	static int Maximo(int numero1, int numero2)
{
    return Math.Max(numero1, numero2);
}
    static int Minimo(int numero1, int numero2)
{
    return Math.Min(numero1, numero2);
}
static Boolean EsPrimo(int numero, Boolean Primo)
{
            int contadorDivisores = 0;

            for (int i = 1; i <= numero; i++)
            { 

                if (numero % i == 0)
                {
                    contadorDivisores++;
                }
            }

            if (contadorDivisores == 2)
            {
                Console.WriteLine("El numero es primo");
            }
            else
            {
                Console.WriteLine("El numero no es primo");
            }
            return Primo;
}
static int Suma(int numeroUno, int numeroDos)
        {
            return numeroUno + numeroDos;
        }
    }
}
