package practica24;
import java.util.Scanner;
public class Practica24 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner lector=new Scanner(System.in);
		System.out.println("Introduce una opcion: \n1)Maximo entre 2 numeros \n2)Minimo entre 2 numeros \n3)Averiguar si un numero es primo \n4)Suma de dos numeros");
		int opcion=lector.nextInt();
		int numero1;
		int numero2;
		
	switch (opcion) {
		case 1:
			System.out.println("Introduce un numero");
			numero1=lector.nextInt();
			System.out.println("Introduce un numero");
			numero2=lector.nextInt();
			System.out.println("El numero maximo es : " + maximo(numero1, numero2));
			break;
		case 2:
			System.out.println("Introduce un numero");
			numero1=lector.nextInt();
			System.out.println("Introduce un numero");
			numero2=lector.nextInt();
			System.out.println("El numero minimo es :" + minimo(numero1, numero2));
			break;
		case 3:
			System.out.println("Introduce un numero");
			numero1=lector.nextInt();
			System.out.println(esPrimo(numero1, true));
			break;
		case 4:
			System.out.println("Introduce un numero");
			numero1=lector.nextInt();
			
			System.out.println("Introduce otro numero");
			numero2=lector.nextInt();
			
			System.out.println("El resultado es " + suma(numero1, numero2));
			break;
	}
	}
	static int maximo( int numero1, int numero2) {
		return Math.max(numero1, numero2);
	}
	static int minimo (int numero1, int numero2) {
		return Math.min(numero1, numero2);
	}
	static boolean esPrimo(int numero, boolean Primo) {
		int contadorDivisores=0;
		
		
		for (int i = 1; i <= numero; i++) {
			
			if(numero%i==0)  {
				contadorDivisores++;
			}
		}
		
		if(contadorDivisores==2) {
			System.out.println("El numero es primo");
		}else {
			System.out.println("El numero no es primo");
		}
		return Primo;
	}
	static int suma (int numeroUno, int numeroDos) {
		return numeroUno+numeroDos;
	}
}
