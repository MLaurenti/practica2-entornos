﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    
   public class ClaseNumeros
        {
            public static int redondearAlza(double a)
            {
                if (a != (int)a)
                {
                    return (int)a + 1;
                }
                return (int)a;

            }

            public static int potencia(int Base, int exponente)
            {
                int resultado = 1;
                for (int i = 0; i < exponente; i++)
                {
                    resultado = resultado * Base;

                }
                return resultado;
            }
            public static double minimo(double a, double b)
            {
                if (a > b)
                {
                    return b;
                }
                else
                {
                    return a;
                }
            }
            public static int redondear(double a)
            {

                if (a - (int)a >= 0.5)
                {
                    return (int)a + 1;
                }
                return (int)a;
            }


            public static int minimo(int a, int b)
            {
                if (a > b)
                {
                    return b;
                }
                else
                {
                    return a;
                }
            }
        }
    }
