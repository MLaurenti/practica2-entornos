﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ClassLibrary2
{
    public class ClaseString
    {

        public static bool esEntero(String cadenaLeida)
        {

            char caracteres;
            int contadorCifras = 0;
            for (int i = 0; i < cadenaLeida.Length; i++)
            {
                caracteres = cadenaLeida[i];
                if (cadenaLeida[0] == '-' && cadenaLeida.Length == 1)
                {
                    return false;
                }
                if (i == 0 && caracteres != '-' && caracteres > '0' && caracteres > '9')
                {
                    return false;
                }
                if (caracteres < '0' && caracteres > '9' && i != 0)
                {
                    return false;
                }

            }

            return true;

        }
        public static String cifrar(String cadena, int desfase)
        {
            String resultado = "";
            char caracteres;
            int conversion = 0;
            for (int i = 0; i < cadena.Length; i++)
            {
                caracteres = cadena[i];

                if (caracteres >= 'a' && caracteres <= 'z')
                {
                    conversion = (int)caracteres + desfase;
                }
                resultado = resultado + (char)conversion;
            }
            return resultado;
        }
        public static void caracterNumero(char caracter)
        {



            if (caracter >= 49 && caracter <= 57)
            {
                Console.WriteLine("EL caracter es un numero");
            }
            else
            {
                Console.WriteLine("El caracter no es un numero");
            }

        }


        public static void numeroCaracteres(String cadena)
        {

            int contadorCaracteres=0;
            
            for (int i = 0; i < cadena.Length; i++)
            {
                
                
                    contadorCaracteres++;
                
            }
            
     
            Console.WriteLine("La cadena tiene " + contadorCaracteres + "caracteres");
            
          
        }
        public static String invertirCadena(String cadena)
        {
            String completa = "";

            for (int i = 0; i < cadena.Length; i++)
            {
                completa = cadena[i] + completa;


            }
            return completa + " ";
        }
        
    }
}

    


