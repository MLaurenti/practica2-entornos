package practics25Libreria;
import libreriaJavaMariaLaurenti.ClaseNumeros;

import libreriaJavaMariaLaurenti.ClaseString;
public class Libreria {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("El numero redondeado es: " +ClaseNumeros.redondear(8.6));
		
		System.out.println("El minimo entre los 2 es: " + ClaseNumeros.minimo(12, 6));
		
		
		System.out.println("El minimo entre los 2 es: " + ClaseNumeros.minimo(2.6, 3.7));
		
		
		System.out.println("El resultado de la potencia es : " + ClaseNumeros.potencia(5, 2));
		
		
		System.out.println("El resultado del redondeo es : " + ClaseNumeros.redondearAlza(2.8));
		
		
		
		System.out.println("La cadena resultante es : " + ClaseString.cifrar("entornos", 2));
		
		
		ClaseString.caracterNumero('8');
		
		
		System.out.println("�Es un numero entero? " +ClaseString.esEntero("8"));
		
	
		System.out.println("La cadena invertida es : " + ClaseString.invertirCadena("playa"));
		
	
		System.out.println("La palabra mas larga es : " + ClaseString.palabraMasLarga("Esto es una practica"));
		
	
	}

}
