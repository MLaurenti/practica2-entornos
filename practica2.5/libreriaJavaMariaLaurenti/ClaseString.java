 package libreriaJavaMariaLaurenti;

import java.util.Scanner;

public class ClaseString {
	public static boolean esEntero(String cadenaLeida) {
		
		char caracteres;
		int contadorCifras=0;
		for (int i = 0; i < cadenaLeida.length(); i++) {
			caracteres=cadenaLeida.charAt(i);
			if(cadenaLeida.charAt(0)=='-' && cadenaLeida.length()==1) {
				return false;
			}
			if(i==0 && caracteres!='-' && caracteres>'0' && caracteres>'9') {
				return false;
			}
			if(caracteres<'0' && caracteres>'9'&& i!=0) {
				return false;
			}
			
		}
		
			return true;
		
		}
	public static String cifrar(String cadena, int desfase) {
		String resultado="";
		char caracteres;
		int conversion=0;
		for (int i = 0; i < cadena.length(); i++) {
			caracteres=cadena.charAt(i);
			
			if(caracteres>='a' && caracteres<='z') {
				conversion=(int)caracteres+desfase;
			}
			resultado= resultado + (char) conversion;
		}
		return resultado;
	}
	public static void caracterNumero(char caracter) {
		
		

		if(caracter>=49 && caracter<=57){
			System.out.println("EL caracter es un numero");
		}else {
			System.out.println("El caracter no es un numero");
		}
		
	}
		
	
	
	public static String palabraMasLarga(String cadena) {
		
		
		String palabra,cadenaMayor="";
		char caracteres;
		int posicionInicial=0;
		cadena= cadena + " ";
		for (int i = 0; i < cadena.length(); i++) {
			caracteres=cadena.charAt(i);
			if(caracteres==' ') {
				palabra=cadena.substring(posicionInicial, i+1);
				if(palabra.length()>cadenaMayor.length()) {
					cadenaMayor=palabra;
				}
				posicionInicial=i+1;
			}
		}
		
		return cadenaMayor;
	}
	public static String invertirCadena(String cadena) {
		String completa="";
		
		for (int i =0; i<cadena.length(); i++) {
			completa=cadena.charAt(i)+ completa;
			
			
		}
	return completa + " ";
	}
}
