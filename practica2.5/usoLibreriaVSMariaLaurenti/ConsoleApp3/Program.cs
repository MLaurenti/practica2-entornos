﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp3
{
    class Program
    {
        static void Main(string[] args)
        {


            Console.WriteLine("El numero redondeado es: " + ClassLibrary2.ClaseNumeros.redondear(8.6));

            Console.WriteLine("El minimo entre los 2 es: " + ClassLibrary2.ClaseNumeros.minimo(12, 6));


            Console.WriteLine("El minimo entre los 2 es: " + ClassLibrary2.ClaseNumeros.minimo(2.6, 3.7));


            Console.WriteLine("El resultado de la potencia es : " + ClassLibrary2.ClaseNumeros.potencia(5, 2));


            Console.WriteLine("El resultado del redondeo es : " + ClassLibrary2.ClaseNumeros.redondearAlza(2.8));



            Console.WriteLine("La cadena resultante es : " + ClassLibrary2.ClaseString.cifrar("entornos", 2));



            ClassLibrary2.ClaseString.caracterNumero('8');


            Console.WriteLine("¿Es un numero entero? " + ClassLibrary2.ClaseString.esEntero("8"));

            
            Console.WriteLine("La cadena invertida es : " + ClassLibrary2.ClaseString.invertirCadena("playa"));


            ClassLibrary2.ClaseString.numeroCaracteres("Esto es una practica de entornos");
            Console.WriteLine("Pulse una tecla para continuar");
            Console.ReadKey();
        }
    }
}
    

