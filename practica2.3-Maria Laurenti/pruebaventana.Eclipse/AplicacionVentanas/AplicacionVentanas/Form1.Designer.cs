﻿namespace AplicacionVentanas
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.trackBar1 = new System.Windows.Forms.TrackBar();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.inicioToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofertasDelDiaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.novedadesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.ofertasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.próximasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.activasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miPedidoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.volverAPedirUnPedidoAntiguoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verDetallesAntoguosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.verDetallesDeLosProductosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.miCuentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.inicioDeSesionYSeguridadToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.misPedidosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.direccionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.gestionarOpcionesDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.productosVistosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.perfilToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.tusMensajesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.configuraciónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.paisesEIdiomasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.borrarCuentaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cerrarSesiónToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.atenciónAlClienteToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.devolucionesYReembolsoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.opcionesDePagoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.cOntactoToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox4 = new System.Windows.Forms.TextBox();
            this.textBox5 = new System.Windows.Forms.TextBox();
            this.textBox6 = new System.Windows.Forms.TextBox();
            this.textBox7 = new System.Windows.Forms.TextBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.button1 = new System.Windows.Forms.Button();
            this.textBox9 = new System.Windows.Forms.TextBox();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.textBox8 = new System.Windows.Forms.TextBox();
            this.button3 = new System.Windows.Forms.Button();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.textBox12 = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            this.menuStrip1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // trackBar1
            // 
            this.trackBar1.AllowDrop = true;
            this.trackBar1.Location = new System.Drawing.Point(173, 499);
            this.trackBar1.Maximum = 100;
            this.trackBar1.Name = "trackBar1";
            this.trackBar1.RightToLeftLayout = true;
            this.trackBar1.Size = new System.Drawing.Size(265, 69);
            this.trackBar1.TabIndex = 0;
            this.trackBar1.TickFrequency = 20;
            this.trackBar1.Value = 50;
            this.trackBar1.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.Location = new System.Drawing.Point(219, 210);
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(120, 26);
            this.numericUpDown1.TabIndex = 1;
            // 
            // menuStrip1
            // 
            this.menuStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioToolStripMenuItem,
            this.ofertasToolStripMenuItem,
            this.miPedidoToolStripMenuItem,
            this.miCuentaToolStripMenuItem,
            this.configuraciónToolStripMenuItem,
            this.atenciónAlClienteToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(734, 30);
            this.menuStrip1.TabIndex = 2;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // inicioToolStripMenuItem
            // 
            this.inicioToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.ofertasDelDiaToolStripMenuItem,
            this.novedadesToolStripMenuItem});
            this.inicioToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.inicioToolStripMenuItem.Name = "inicioToolStripMenuItem";
            this.inicioToolStripMenuItem.Size = new System.Drawing.Size(70, 26);
            this.inicioToolStripMenuItem.Text = "Inicio";
            // 
            // ofertasDelDiaToolStripMenuItem
            // 
            this.ofertasDelDiaToolStripMenuItem.Name = "ofertasDelDiaToolStripMenuItem";
            this.ofertasDelDiaToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.ofertasDelDiaToolStripMenuItem.Text = "Ofertas del dia";
            // 
            // novedadesToolStripMenuItem
            // 
            this.novedadesToolStripMenuItem.Name = "novedadesToolStripMenuItem";
            this.novedadesToolStripMenuItem.Size = new System.Drawing.Size(221, 30);
            this.novedadesToolStripMenuItem.Text = "Novedades";
            // 
            // ofertasToolStripMenuItem
            // 
            this.ofertasToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.próximasToolStripMenuItem,
            this.activasToolStripMenuItem});
            this.ofertasToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ofertasToolStripMenuItem.Name = "ofertasToolStripMenuItem";
            this.ofertasToolStripMenuItem.Size = new System.Drawing.Size(85, 26);
            this.ofertasToolStripMenuItem.Text = "Ofertas";
            // 
            // próximasToolStripMenuItem
            // 
            this.próximasToolStripMenuItem.Name = "próximasToolStripMenuItem";
            this.próximasToolStripMenuItem.Size = new System.Drawing.Size(171, 30);
            this.próximasToolStripMenuItem.Text = "Próximas";
            // 
            // activasToolStripMenuItem
            // 
            this.activasToolStripMenuItem.Name = "activasToolStripMenuItem";
            this.activasToolStripMenuItem.Size = new System.Drawing.Size(171, 30);
            this.activasToolStripMenuItem.Text = "Activas";
            // 
            // miPedidoToolStripMenuItem
            // 
            this.miPedidoToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.volverAPedirUnPedidoAntiguoToolStripMenuItem,
            this.verDetallesAntoguosToolStripMenuItem,
            this.verDetallesDeLosProductosToolStripMenuItem});
            this.miPedidoToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miPedidoToolStripMenuItem.Name = "miPedidoToolStripMenuItem";
            this.miPedidoToolStripMenuItem.Size = new System.Drawing.Size(127, 26);
            this.miPedidoToolStripMenuItem.Text = "Mis pedidos";
            // 
            // volverAPedirUnPedidoAntiguoToolStripMenuItem
            // 
            this.volverAPedirUnPedidoAntiguoToolStripMenuItem.Name = "volverAPedirUnPedidoAntiguoToolStripMenuItem";
            this.volverAPedirUnPedidoAntiguoToolStripMenuItem.Size = new System.Drawing.Size(384, 30);
            this.volverAPedirUnPedidoAntiguoToolStripMenuItem.Text = "Volver a pedir un pedido antiguo";
            // 
            // verDetallesAntoguosToolStripMenuItem
            // 
            this.verDetallesAntoguosToolStripMenuItem.Name = "verDetallesAntoguosToolStripMenuItem";
            this.verDetallesAntoguosToolStripMenuItem.Size = new System.Drawing.Size(384, 30);
            this.verDetallesAntoguosToolStripMenuItem.Text = "Ver pedidos antiguos";
            this.verDetallesAntoguosToolStripMenuItem.Click += new System.EventHandler(this.verDetallesAntoguosToolStripMenuItem_Click);
            // 
            // verDetallesDeLosProductosToolStripMenuItem
            // 
            this.verDetallesDeLosProductosToolStripMenuItem.Name = "verDetallesDeLosProductosToolStripMenuItem";
            this.verDetallesDeLosProductosToolStripMenuItem.Size = new System.Drawing.Size(384, 30);
            this.verDetallesDeLosProductosToolStripMenuItem.Text = "Ver detalles de los productos";
            // 
            // miCuentaToolStripMenuItem
            // 
            this.miCuentaToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.inicioDeSesionYSeguridadToolStripMenuItem,
            this.misPedidosToolStripMenuItem,
            this.direccionesToolStripMenuItem,
            this.gestionarOpcionesDePagoToolStripMenuItem,
            this.productosVistosToolStripMenuItem,
            this.perfilToolStripMenuItem,
            this.tusMensajesToolStripMenuItem});
            this.miCuentaToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.miCuentaToolStripMenuItem.Name = "miCuentaToolStripMenuItem";
            this.miCuentaToolStripMenuItem.Size = new System.Drawing.Size(110, 26);
            this.miCuentaToolStripMenuItem.Text = "Mi cuenta";
            // 
            // inicioDeSesionYSeguridadToolStripMenuItem
            // 
            this.inicioDeSesionYSeguridadToolStripMenuItem.Name = "inicioDeSesionYSeguridadToolStripMenuItem";
            this.inicioDeSesionYSeguridadToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.inicioDeSesionYSeguridadToolStripMenuItem.Text = "Inicio de sesion y seguridad";
            // 
            // misPedidosToolStripMenuItem
            // 
            this.misPedidosToolStripMenuItem.Name = "misPedidosToolStripMenuItem";
            this.misPedidosToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.misPedidosToolStripMenuItem.Text = "Mis pedidos";
            // 
            // direccionesToolStripMenuItem
            // 
            this.direccionesToolStripMenuItem.Name = "direccionesToolStripMenuItem";
            this.direccionesToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.direccionesToolStripMenuItem.Text = "Direcciones";
            // 
            // gestionarOpcionesDePagoToolStripMenuItem
            // 
            this.gestionarOpcionesDePagoToolStripMenuItem.Name = "gestionarOpcionesDePagoToolStripMenuItem";
            this.gestionarOpcionesDePagoToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.gestionarOpcionesDePagoToolStripMenuItem.Text = "Gestionar opciones de pago";
            // 
            // productosVistosToolStripMenuItem
            // 
            this.productosVistosToolStripMenuItem.Name = "productosVistosToolStripMenuItem";
            this.productosVistosToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.productosVistosToolStripMenuItem.Text = "Productos vistos";
            // 
            // perfilToolStripMenuItem
            // 
            this.perfilToolStripMenuItem.Name = "perfilToolStripMenuItem";
            this.perfilToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.perfilToolStripMenuItem.Text = "Perfil";
            // 
            // tusMensajesToolStripMenuItem
            // 
            this.tusMensajesToolStripMenuItem.Name = "tusMensajesToolStripMenuItem";
            this.tusMensajesToolStripMenuItem.Size = new System.Drawing.Size(347, 30);
            this.tusMensajesToolStripMenuItem.Text = "Tus mensajes";
            // 
            // configuraciónToolStripMenuItem
            // 
            this.configuraciónToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.notificacionesToolStripMenuItem,
            this.paisesEIdiomasToolStripMenuItem,
            this.borrarCuentaToolStripMenuItem,
            this.cerrarSesiónToolStripMenuItem});
            this.configuraciónToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.configuraciónToolStripMenuItem.Name = "configuraciónToolStripMenuItem";
            this.configuraciónToolStripMenuItem.Size = new System.Drawing.Size(148, 26);
            this.configuraciónToolStripMenuItem.Text = "Configuración";
            // 
            // notificacionesToolStripMenuItem
            // 
            this.notificacionesToolStripMenuItem.Name = "notificacionesToolStripMenuItem";
            this.notificacionesToolStripMenuItem.Size = new System.Drawing.Size(229, 30);
            this.notificacionesToolStripMenuItem.Text = "Notificaciones";
            // 
            // paisesEIdiomasToolStripMenuItem
            // 
            this.paisesEIdiomasToolStripMenuItem.Name = "paisesEIdiomasToolStripMenuItem";
            this.paisesEIdiomasToolStripMenuItem.Size = new System.Drawing.Size(229, 30);
            this.paisesEIdiomasToolStripMenuItem.Text = "Paises e idioma";
            // 
            // borrarCuentaToolStripMenuItem
            // 
            this.borrarCuentaToolStripMenuItem.Name = "borrarCuentaToolStripMenuItem";
            this.borrarCuentaToolStripMenuItem.Size = new System.Drawing.Size(229, 30);
            this.borrarCuentaToolStripMenuItem.Text = "Borrar cuenta";
            // 
            // cerrarSesiónToolStripMenuItem
            // 
            this.cerrarSesiónToolStripMenuItem.Name = "cerrarSesiónToolStripMenuItem";
            this.cerrarSesiónToolStripMenuItem.Size = new System.Drawing.Size(229, 30);
            this.cerrarSesiónToolStripMenuItem.Text = "Cerrar sesión";
            // 
            // atenciónAlClienteToolStripMenuItem
            // 
            this.atenciónAlClienteToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.devolucionesYReembolsoToolStripMenuItem,
            this.opcionesDePagoToolStripMenuItem,
            this.cOntactoToolStripMenuItem});
            this.atenciónAlClienteToolStripMenuItem.Font = new System.Drawing.Font("Century Gothic", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.atenciónAlClienteToolStripMenuItem.Name = "atenciónAlClienteToolStripMenuItem";
            this.atenciónAlClienteToolStripMenuItem.Size = new System.Drawing.Size(188, 26);
            this.atenciónAlClienteToolStripMenuItem.Text = "Atención al cliente";
            // 
            // devolucionesYReembolsoToolStripMenuItem
            // 
            this.devolucionesYReembolsoToolStripMenuItem.Name = "devolucionesYReembolsoToolStripMenuItem";
            this.devolucionesYReembolsoToolStripMenuItem.Size = new System.Drawing.Size(328, 30);
            this.devolucionesYReembolsoToolStripMenuItem.Text = "Devoluciones y reembolso";
            // 
            // opcionesDePagoToolStripMenuItem
            // 
            this.opcionesDePagoToolStripMenuItem.Name = "opcionesDePagoToolStripMenuItem";
            this.opcionesDePagoToolStripMenuItem.Size = new System.Drawing.Size(328, 30);
            this.opcionesDePagoToolStripMenuItem.Text = "Opciones de pago";
            // 
            // cOntactoToolStripMenuItem
            // 
            this.cOntactoToolStripMenuItem.Name = "cOntactoToolStripMenuItem";
            this.cOntactoToolStripMenuItem.Size = new System.Drawing.Size(328, 30);
            this.cOntactoToolStripMenuItem.Text = "COntacto";
            // 
            // textBox1
            // 
            this.textBox1.BackColor = System.Drawing.Color.Coral;
            this.textBox1.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox1.Location = new System.Drawing.Point(141, 94);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(147, 25);
            this.textBox1.TabIndex = 3;
            this.textBox1.Text = "INICIAR SESION";
            this.textBox1.TextChanged += new System.EventHandler(this.textBox1_TextChanged);
            // 
            // textBox2
            // 
            this.textBox2.BackColor = System.Drawing.Color.Coral;
            this.textBox2.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox2.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox2.Location = new System.Drawing.Point(141, 341);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(147, 25);
            this.textBox2.TabIndex = 4;
            this.textBox2.Text = "BUSQUEDA";
            // 
            // textBox4
            // 
            this.textBox4.Location = new System.Drawing.Point(219, 146);
            this.textBox4.Name = "textBox4";
            this.textBox4.Size = new System.Drawing.Size(226, 26);
            this.textBox4.TabIndex = 6;
            this.textBox4.TextChanged += new System.EventHandler(this.textBox4_TextChanged);
            // 
            // textBox5
            // 
            this.textBox5.BackColor = System.Drawing.Color.Coral;
            this.textBox5.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox5.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox5.ForeColor = System.Drawing.Color.Black;
            this.textBox5.Location = new System.Drawing.Point(39, 212);
            this.textBox5.Name = "textBox5";
            this.textBox5.Size = new System.Drawing.Size(147, 20);
            this.textBox5.TabIndex = 7;
            this.textBox5.Text = "Edad";
            // 
            // textBox6
            // 
            this.textBox6.BackColor = System.Drawing.Color.Coral;
            this.textBox6.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox6.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox6.ForeColor = System.Drawing.Color.Black;
            this.textBox6.Location = new System.Drawing.Point(36, 184);
            this.textBox6.Name = "textBox6";
            this.textBox6.Size = new System.Drawing.Size(147, 20);
            this.textBox6.TabIndex = 8;
            this.textBox6.Text = "Contraseña";
            // 
            // textBox7
            // 
            this.textBox7.BackColor = System.Drawing.Color.Coral;
            this.textBox7.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox7.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox7.ForeColor = System.Drawing.Color.Black;
            this.textBox7.Location = new System.Drawing.Point(36, 152);
            this.textBox7.Name = "textBox7";
            this.textBox7.Size = new System.Drawing.Size(147, 20);
            this.textBox7.TabIndex = 9;
            this.textBox7.Text = "Usuario";
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.checkBox1.ForeColor = System.Drawing.Color.Black;
            this.checkBox1.Location = new System.Drawing.Point(73, 255);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(349, 23);
            this.checkBox1.TabIndex = 10;
            this.checkBox1.Text = "Guardar informacion del inicio de sesión";
            this.checkBox1.UseVisualStyleBackColor = true;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.checkBox1_CheckedChanged);
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(173, 284);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(166, 51);
            this.button1.TabIndex = 11;
            this.button1.Text = "Iniciar sesión";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // textBox9
            // 
            this.textBox9.Location = new System.Drawing.Point(219, 372);
            this.textBox9.Name = "textBox9";
            this.textBox9.Size = new System.Drawing.Size(239, 26);
            this.textBox9.TabIndex = 13;
            // 
            // textBox10
            // 
            this.textBox10.BackColor = System.Drawing.Color.Coral;
            this.textBox10.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox10.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.ForeColor = System.Drawing.Color.Black;
            this.textBox10.Location = new System.Drawing.Point(17, 421);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(249, 20);
            this.textBox10.TabIndex = 14;
            this.textBox10.Text = "Buscar producto por categoría";
            // 
            // textBox11
            // 
            this.textBox11.BackColor = System.Drawing.Color.Coral;
            this.textBox11.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox11.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.ForeColor = System.Drawing.Color.Black;
            this.textBox11.Location = new System.Drawing.Point(36, 378);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(160, 20);
            this.textBox11.TabIndex = 15;
            this.textBox11.Text = "Buscar producto";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(496, 360);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(166, 38);
            this.button2.TabIndex = 16;
            this.button2.Text = "Aceptar";
            this.button2.UseVisualStyleBackColor = true;
            // 
            // comboBox1
            // 
            this.comboBox1.DisplayMember = "Apps y Juegos";
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Items.AddRange(new object[] {
            "Apps y Juegos",
            "Belleza",
            "Bricolaje",
            "Deportes",
            "Electrónica",
            "Hogar",
            "Informática",
            "Moda",
            "Medicina"});
            this.comboBox1.Location = new System.Drawing.Point(305, 413);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(153, 28);
            this.comboBox1.TabIndex = 17;
            this.comboBox1.Text = "Apps y Juegos";
            this.comboBox1.ValueMember = "Apps y Juegos";
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton1.ForeColor = System.Drawing.Color.Black;
            this.radioButton1.Location = new System.Drawing.Point(173, 458);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(93, 25);
            this.radioButton1.TabIndex = 18;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "Ofertas";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.radioButton2.ForeColor = System.Drawing.Color.Black;
            this.radioButton2.Location = new System.Drawing.Point(330, 458);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(74, 25);
            this.radioButton2.TabIndex = 19;
            this.radioButton2.TabStop = true;
            this.radioButton2.Text = "Todo";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // textBox8
            // 
            this.textBox8.BackColor = System.Drawing.Color.Coral;
            this.textBox8.BorderStyle = System.Windows.Forms.BorderStyle.None;
            this.textBox8.Font = new System.Drawing.Font("Century Gothic", 8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox8.ForeColor = System.Drawing.Color.Black;
            this.textBox8.Location = new System.Drawing.Point(12, 499);
            this.textBox8.Name = "textBox8";
            this.textBox8.Size = new System.Drawing.Size(160, 20);
            this.textBox8.TabIndex = 20;
            this.textBox8.Text = "Precio";
            this.textBox8.TextChanged += new System.EventHandler(this.textBox8_TextChanged);
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Century Gothic", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(205, 560);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(166, 50);
            this.button3.TabIndex = 21;
            this.button3.Text = "Buscar";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // toolStrip1
            // 
            this.toolStrip1.ImageScalingSize = new System.Drawing.Size(24, 24);
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripButton3});
            this.toolStrip1.Location = new System.Drawing.Point(0, 30);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(734, 31);
            this.toolStrip1.TabIndex = 22;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton1.Image = global::AplicacionVentanas.Properties.Resources.icons8_carrito_de_compras_24;
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton1.Text = "toolStripButton1";
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton2.Image = global::AplicacionVentanas.Properties.Resources.icons8_ajustes_24;
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton2.Text = "toolStripButton2";
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Image;
            this.toolStripButton3.Image = global::AplicacionVentanas.Properties.Resources.icons8_salir_redondeado_24;
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(28, 28);
            this.toolStripButton3.Text = "toolStripButton3";
            // 
            // textBox12
            // 
            this.textBox12.Location = new System.Drawing.Point(219, 178);
            this.textBox12.Name = "textBox12";
            this.textBox12.Size = new System.Drawing.Size(226, 26);
            this.textBox12.TabIndex = 23;
            this.textBox12.UseSystemPasswordChar = true;
            this.textBox12.TextChanged += new System.EventHandler(this.textBox12_TextChanged);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 20F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Coral;
            this.ClientSize = new System.Drawing.Size(734, 622);
            this.Controls.Add(this.textBox12);
            this.Controls.Add(this.textBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.textBox8);
            this.Controls.Add(this.radioButton2);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.textBox9);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.textBox7);
            this.Controls.Add(this.textBox6);
            this.Controls.Add(this.textBox5);
            this.Controls.Add(this.textBox4);
            this.Controls.Add(this.textBox2);
            this.Controls.Add(this.numericUpDown1);
            this.Controls.Add(this.trackBar1);
            this.Controls.Add(this.menuStrip1);
            this.ForeColor = System.Drawing.Color.Maroon;
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "Clickme!";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.trackBar1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TrackBar trackBar1;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem inicioToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ofertasDelDiaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem novedadesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem ofertasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem próximasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem activasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miPedidoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem volverAPedirUnPedidoAntiguoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verDetallesAntoguosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem verDetallesDeLosProductosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem miCuentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem configuraciónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem atenciónAlClienteToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem inicioDeSesionYSeguridadToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem misPedidosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem direccionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gestionarOpcionesDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem productosVistosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem perfilToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem tusMensajesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem notificacionesToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem paisesEIdiomasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem borrarCuentaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cerrarSesiónToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem devolucionesYReembolsoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem opcionesDePagoToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem cOntactoToolStripMenuItem;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox4;
        private System.Windows.Forms.TextBox textBox5;
        private System.Windows.Forms.TextBox textBox6;
        private System.Windows.Forms.TextBox textBox7;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.TextBox textBox9;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.TextBox textBox8;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.TextBox textBox12;
    }
}

