package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JButton;
import javax.swing.JTextField;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import java.awt.Color;
import javax.swing.JSpinner;
import javax.swing.JComboBox;
import javax.swing.DefaultComboBoxModel;
import javax.swing.JRadioButton;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.Window.Type;
import javax.swing.JSlider;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import java.awt.GridLayout;
import javax.swing.ImageIcon;
import java.awt.Insets;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;
import javax.swing.border.MatteBorder;
import javax.swing.border.SoftBevelBorder;
import javax.swing.border.BevelBorder;
import javax.swing.border.TitledBorder;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EtchedBorder;
import javax.swing.UIManager;
import javax.swing.JCheckBox;
import javax.swing.JToolBar;
import java.awt.Button;
/**
 * @author Maria Laurenti I�igo
 * 
 * @since 8-1-2018
 */
public class Clickme extends JFrame {

	private JPanel contentPane;
	private JTextField textField;
	private JPasswordField passwordField;
	private JTextField textField_1;
	private JTextField txtEdad;

	/**
	 *
	 * 
	 * 
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Clickme frame = new Clickme();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public Clickme() {
		setFont(new Font("Century Gothic", Font.PLAIN, 12));
		setForeground(new Color(250, 250, 210));
		setBackground(new Color(250, 250, 210));
		setTitle("Clickme!");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 669, 726);
		
		JMenuBar menuBar = new JMenuBar();
		menuBar.setBackground(new Color(245, 222, 179));
		menuBar.setFont(new Font("Century Gothic", Font.PLAIN, 25));
		menuBar.setMargin(new Insets(0, 30, 0, 0));
		menuBar.setForeground(new Color(245, 222, 179));
		setJMenuBar(menuBar);
		
		JMenu mnArchivo = new JMenu("Inicio");
		mnArchivo.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(128, 0, 0)));
		mnArchivo.setForeground(new Color(128, 0, 0));
		mnArchivo.setFont(new Font("Century Gothic", Font.BOLD, 18));
		mnArchivo.setBackground(new Color(245, 222, 179));
		menuBar.add(mnArchivo);
		
		JMenuItem mntmOfertasDelDa = new JMenuItem("Ofertas del d\u00EDa");
		mntmOfertasDelDa.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnArchivo.add(mntmOfertasDelDa);
		
		JMenuItem mntmNovedades = new JMenuItem("Novedades");
		mntmNovedades.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnArchivo.add(mntmNovedades);
		
		JMenu mnNewMenu = new JMenu("Ofertas");
		mnNewMenu.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "", TitledBorder.LEADING, TitledBorder.TOP, null, new Color(128, 0, 0)));
		mnNewMenu.setForeground(new Color(128, 0, 0));
		mnNewMenu.setFont(new Font("Century Gothic", Font.BOLD, 18));
		menuBar.add(mnNewMenu);
		
		JMenuItem mntmPrximas = new JMenuItem("Pr\u00F3ximas");
		mntmPrximas.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnNewMenu.add(mntmPrximas);
		
		JMenuItem mntmActivas = new JMenuItem("Activas");
		mntmActivas.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnNewMenu.add(mntmActivas);
		
		JMenu mnMisPedidos = new JMenu("Mis pedidos");
		mnMisPedidos.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mnMisPedidos.setForeground(new Color(128, 0, 0));
		mnMisPedidos.setFont(new Font("Century Gothic", Font.BOLD, 18));
		menuBar.add(mnMisPedidos);
		
		JMenuItem mntmVolverAPedir = new JMenuItem("Volver a pedir un pedido antiguo");
		mntmVolverAPedir.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMisPedidos.add(mntmVolverAPedir);
		
		JMenuItem mntmVerDetallesDe = new JMenuItem("Ver detalles de los productos ");
		mntmVerDetallesDe.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMisPedidos.add(mntmVerDetallesDe);
		
		JMenuItem mntmVerPedidosAntiguos = new JMenuItem("Ver pedidos antiguos");
		mntmVerPedidosAntiguos.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMisPedidos.add(mntmVerPedidosAntiguos);
		
		JMenu mnMiCuenta = new JMenu("Mi cuenta");
		mnMiCuenta.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mnMiCuenta.setForeground(new Color(128, 0, 0));
		mnMiCuenta.setFont(new Font("Century Gothic", Font.BOLD, 18));
		menuBar.add(mnMiCuenta);
		
		JMenuItem mntmInicioDeSesion = new JMenuItem("Inicio de sesion y seguridad");
		mntmInicioDeSesion.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmInicioDeSesion);
		
		JMenuItem mntmMisPedidos = new JMenuItem("Mis pedidos");
		mntmMisPedidos.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmMisPedidos);
		
		JMenuItem mntmDirecciones = new JMenuItem("Direcciones");
		mntmDirecciones.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmDirecciones);
		
		JMenuItem mntmGestionarOpcionesDe = new JMenuItem("Gestionar opciones de pago");
		mntmGestionarOpcionesDe.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmGestionarOpcionesDe);
		
		JMenuItem mntmProductosVistos = new JMenuItem("Productos vistos");
		mntmProductosVistos.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmProductosVistos);
		
		JMenuItem mntmPerfil = new JMenuItem("Perfil");
		mntmPerfil.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmPerfil);
		
		JMenuItem mntmTusMensajes = new JMenuItem("Tus mensajes");
		mntmTusMensajes.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnMiCuenta.add(mntmTusMensajes);
		
		JMenu mnConfiguracin = new JMenu("Configuraci\u00F3n");
		mnConfiguracin.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mnConfiguracin.setForeground(new Color(128, 0, 0));
		mnConfiguracin.setFont(new Font("Century Gothic", Font.BOLD, 18));
		menuBar.add(mnConfiguracin);
		
		JMenuItem mntmNotificaciones = new JMenuItem("Notificaciones");
		mntmNotificaciones.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnConfiguracin.add(mntmNotificaciones);
		
		JMenuItem mntmPaisEIdioma = new JMenuItem("Pais e idioma");
		mntmPaisEIdioma.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnConfiguracin.add(mntmPaisEIdioma);
		
		JMenuItem mntmBorrarCuenta = new JMenuItem("Borrar cuenta");
		mntmBorrarCuenta.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnConfiguracin.add(mntmBorrarCuenta);
		
		JMenuItem mntmCerrarSesion = new JMenuItem("Cerrar sesion");
		mntmCerrarSesion.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnConfiguracin.add(mntmCerrarSesion);
		
		JMenu mnTencinAlCliente = new JMenu("Atenci\u00F3n al cliente");
		mnTencinAlCliente.setBorder(new TitledBorder(null, "", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		mnTencinAlCliente.setForeground(new Color(128, 0, 0));
		mnTencinAlCliente.setHorizontalAlignment(SwingConstants.RIGHT);
		mnTencinAlCliente.setFont(new Font("Century Gothic", Font.BOLD, 18));
		menuBar.add(mnTencinAlCliente);
		
		JMenuItem mntmDevolucionesYReembolsos = new JMenuItem("Devoluciones y reembolsos");
		mntmDevolucionesYReembolsos.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnTencinAlCliente.add(mntmDevolucionesYReembolsos);
		
		JMenuItem mntmOpcionesDePago = new JMenuItem("Opciones de pago");
		mntmOpcionesDePago.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnTencinAlCliente.add(mntmOpcionesDePago);
		
		JMenuItem mntmContacto = new JMenuItem("Contacto");
		mntmContacto.setFont(new Font("Century Gothic", Font.PLAIN, 18));
		mnTencinAlCliente.add(mntmContacto);
		contentPane = new JPanel();
		contentPane.setBackground(new Color(244, 164, 96));
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JButton btnCancelar = new JButton("Aceptar");
		btnCancelar.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		btnCancelar.setFont(new Font("Century Gothic", Font.BOLD, 17));
		btnCancelar.setBackground(new Color(244, 164, 96));
		btnCancelar.setForeground(new Color(128, 0, 0));
		btnCancelar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnCancelar.setBounds(430, 350, 141, 29);
		contentPane.add(btnCancelar);
		
		textField = new JTextField();
		textField.setBackground(new Color(245, 222, 179));
		textField.setForeground(new Color(245, 222, 179));
		textField.setBounds(165, 352, 235, 26);
		contentPane.add(textField);
		textField.setColumns(10);
		
		JLabel lblNombre = new JLabel("Buscar producto");
		lblNombre.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblNombre.setBounds(15, 354, 141, 20);
		contentPane.add(lblNombre);
		
		JComboBox comboBox = new JComboBox();
		comboBox.setFont(new Font("Century Gothic", Font.BOLD, 16));
		comboBox.setBackground(new Color(245, 222, 179));
		comboBox.setForeground(new Color(128, 0, 0));
		comboBox.setModel(new DefaultComboBoxModel(new String[] {"Apps y juegos", "Belleza", "Bricolaje", "Deportes", "Electr\u00F3nica", "Hogar", "Inform\u00E1tica", "Moda", "Oficina"}));
		comboBox.setBounds(271, 405, 141, 29);
		contentPane.add(comboBox);
		
		JLabel lblBuscarPrendaPor = new JLabel("Buscar producto por categor\u00EDa");
		lblBuscarPrendaPor.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblBuscarPrendaPor.setBounds(15, 409, 251, 20);
		contentPane.add(lblBuscarPrendaPor);
		
		JRadioButton rdbtnNewRadioButton = new JRadioButton("Ofertas");
		rdbtnNewRadioButton.setForeground(new Color(0, 0, 0));
		rdbtnNewRadioButton.setFont(new Font("Century Gothic", Font.BOLD, 16));
		rdbtnNewRadioButton.setBackground(new Color(244, 164, 96));
		rdbtnNewRadioButton.setBounds(151, 460, 104, 29);
		contentPane.add(rdbtnNewRadioButton);
		
		JRadioButton rdbtnTodo = new JRadioButton("Todo");
		rdbtnTodo.setForeground(new Color(0, 0, 0));
		rdbtnTodo.setFont(new Font("Century Gothic", Font.BOLD, 16));
		rdbtnTodo.setBackground(new Color(244, 164, 96));
		rdbtnTodo.setBounds(283, 460, 104, 29);
		contentPane.add(rdbtnTodo);
		
		JButton btnBuscar = new JButton("Buscar");
		btnBuscar.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnBuscar.setFont(new Font("Century Gothic", Font.BOLD, 16));
		btnBuscar.setForeground(new Color(128, 0, 0));
		btnBuscar.setBackground(new Color(244, 164, 96));
		btnBuscar.setBounds(249, 568, 115, 29);
		contentPane.add(btnBuscar);
		
		JLabel lblNewLabel = new JLabel("INICIAR SESION");
		lblNewLabel.setForeground(Color.BLACK);
		lblNewLabel.setFont(new Font("Century Gothic", Font.BOLD, 20));
		lblNewLabel.setBounds(65, 67, 155, 20);
		contentPane.add(lblNewLabel);
		
		JLabel lblUsuario = new JLabel("Usuario");
		lblUsuario.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblUsuario.setBounds(15, 103, 74, 20);
		contentPane.add(lblUsuario);
		
		JLabel lblContrasea = new JLabel("Contrase\u00F1a");
		lblContrasea.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblContrasea.setBounds(15, 139, 104, 20);
		contentPane.add(lblContrasea);
		
		passwordField = new JPasswordField();
		passwordField.setBackground(new Color(245, 222, 179));
		passwordField.setForeground(new Color(245, 222, 179));
		passwordField.setBounds(117, 136, 228, 29);
		contentPane.add(passwordField);
		
		textField_1 = new JTextField();
		textField_1.setBackground(new Color(245, 222, 179));
		textField_1.setForeground(new Color(245, 222, 179));
		textField_1.setColumns(10);
		textField_1.setBounds(117, 103, 228, 26);
		contentPane.add(textField_1);
		
		JLabel lblBsqueda = new JLabel("B\u00DASQUEDA");
		lblBsqueda.setForeground(Color.BLACK);
		lblBsqueda.setFont(new Font("Century Gothic", Font.BOLD, 20));
		lblBsqueda.setBounds(79, 308, 122, 20);
		contentPane.add(lblBsqueda);
		
		JButton btnIniciarSesin = new JButton("Iniciar sesi\u00F3n");
		btnIniciarSesin.setBorder(new BevelBorder(BevelBorder.LOWERED, new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0), new Color(0, 0, 0)));
		btnIniciarSesin.setFont(new Font("Century Gothic", Font.BOLD, 17));
		btnIniciarSesin.setForeground(new Color(128, 0, 0));
		btnIniciarSesin.setBackground(new Color(244, 164, 96));
		btnIniciarSesin.setBounds(133, 252, 147, 29);
		contentPane.add(btnIniciarSesin);
		
		JSlider slider = new JSlider();
		slider.setPaintTrack(false);
		slider.setPaintTicks(true);
		slider.setPaintLabels(true);
		slider.setBorder(new SoftBevelBorder(BevelBorder.LOWERED, new Color(244, 164, 96), new Color(244, 164, 96), new Color(244, 164, 96), new Color(244, 164, 96)));
		slider.setBackground(new Color(244, 164, 96));
		slider.setForeground(new Color(0, 0, 0));
		slider.addChangeListener(new ChangeListener() {
			public void stateChanged(ChangeEvent arg0) {
			}
		});
		slider.setMajorTickSpacing(20);
		slider.setFont(new Font("Century Gothic", Font.BOLD, 16));
		slider.setBounds(120, 504, 363, 65);
		contentPane.add(slider);
		
		JLabel lblPrecio = new JLabel("Precio");
		lblPrecio.setFont(new Font("Century Gothic", Font.BOLD, 16));
		lblPrecio.setBounds(15, 527, 74, 20);
		contentPane.add(lblPrecio);
		
		JLabel label = new JLabel("");
		label.setBounds(211, 326, 69, 20);
		contentPane.add(label);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setBounds(570, 0, 2, 2);
		contentPane.add(scrollPane);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		scrollPane_1.setBounds(587, 372, -36, -370);
		contentPane.add(scrollPane_1);
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Guardar informaci\u00F3n del inicio de sesi\u00F3n");
		chckbxNewCheckBox.setBackground(new Color(244, 164, 96));
		chckbxNewCheckBox.setFont(new Font("Century Gothic", Font.BOLD, 16));
		chckbxNewCheckBox.setBounds(15, 201, 356, 38);
		contentPane.add(chckbxNewCheckBox);
		
		JSpinner spinner = new JSpinner();
		spinner.setFont(new Font("Century Gothic", Font.BOLD, 16));
		spinner.setForeground(new Color(128, 0, 0));
		spinner.setBackground(new Color(244, 164, 96));
		spinner.setBounds(117, 175, 74, 26);
		contentPane.add(spinner);
		
		txtEdad = new JTextField();
		txtEdad.setBackground(new Color(244, 164, 96));
		txtEdad.setFont(new Font("Century Gothic", Font.BOLD, 16));
		txtEdad.setText("Edad");
		txtEdad.setBounds(15, 175, 69, 26);
		contentPane.add(txtEdad);
		txtEdad.setColumns(10);
		
		JToolBar toolBar = new JToolBar();
		toolBar.setBackground(new Color(244, 164, 96));
		toolBar.setBounds(0, 0, 62, 38);
		contentPane.add(toolBar);
		
		JButton btnNewButton_3 = new JButton("");
		btnNewButton_3.setBackground(new Color(244, 164, 96));
		btnNewButton_3.setIcon(new ImageIcon(Clickme.class.getResource("/imagenes/icons8-carrito-de-compras-24.png")));
		toolBar.add(btnNewButton_3);
		
		JToolBar toolBar_1 = new JToolBar();
		toolBar_1.setBackground(new Color(244, 164, 96));
		toolBar_1.setBounds(54, 0, 49, 38);
		contentPane.add(toolBar_1);
		
		JButton btnNewButton_4 = new JButton("");
		btnNewButton_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		btnNewButton_4.setBackground(new Color(244, 164, 96));
		btnNewButton_4.setIcon(new ImageIcon(Clickme.class.getResource("/imagenes/icons8-ajustes-24.png")));
		toolBar_1.add(btnNewButton_4);
		
		JToolBar toolBar_2 = new JToolBar();
		toolBar_2.setBackground(new Color(244, 164, 96));
		toolBar_2.setBounds(114, 0, 61, 38);
		contentPane.add(toolBar_2);
		
		JButton btnNewButton_5 = new JButton("");
		btnNewButton_5.setBackground(new Color(244, 164, 96));
		btnNewButton_5.setIcon(new ImageIcon(Clickme.class.getResource("/imagenes/icons8-salir-redondeado-24.png")));
		toolBar_2.add(btnNewButton_5);
	}
}
