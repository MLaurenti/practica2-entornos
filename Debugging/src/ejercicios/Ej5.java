package ejercicios;

import java.util.Scanner;

public class Ej5 {

	public static void main(String[] args) {
		/*
		 * Ayudate del debugger para entender qué realiza este programa
		 */
		
		
		Scanner lector;
		int numeroLeido;
		int cantidadDivisores = 0;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();
		
		
		for (int i = 1; i <= numeroLeido; i++) {
			if(numeroLeido % i == 0){
				cantidadDivisores++;
			}
		}
		
		if(cantidadDivisores > 2){
			System.out.println("No lo es");
		}else{
			System.out.println("Si lo es");
		}
		/*Es un programa para averiguar si un numero es primo o no lo es.
		 * En 1 lugar se crea una variable llamada contadorDividores que te va a contar el numero de divisores y se inicializa en 0.Luego
		 *  el programa te pide que introduzcas un numero (por ejemplo 3) y lo lee. 
		 *  EN 2�lugar: el programa entra en el bucle --> i vale 1,  y la variable cantidadDivisores se aumenta en una unidad, porque el numero introducido
		 *  es divisible por 1 y el resto da cero, con lo que se cumple la condicion del if y la variable cantidadDivisores aumenta en 1.El programa vuelve
		 *  a comenzar el bucle.
		 *  En 3�lugar: el bucle se inicia por 2�vez por lo que, en esta ocasion, la variable i toma el valor de 2. En este momento realiza la division
		 *  del numero introducido por 2 (valor de la variable i en este momento), como el resto no da cero, vuelve a comenzar el bucle de nuevo. En esta 
		 *  ocasion la variable cantidadDivisores no aumenta y sigue tomando el valor de 1 porque la condicion del if no se cumple esta vez.
		 *  En 4� lugar: el bucle se inicia por 3� vez y la variable i toma el valor de 3. El programa entra dentro de la condicion del if y, como en esta
		 *  vez s� que se cumple (porque 3 dividido para 3 da de resto 0), la variable cantidadDivisores se aumenta en una unidad, tomando finalmente
		 *  el valor de 2  
		 *  POr �ltimo lugar el programa sale del bucle y entra en la condicion del if de la linea de codigo 28. Como ya hemos dicho la variable 
		 *  cantidadDivisores tiene el valor de 2 por lo que no cumple la condicion del if que se cumpliria solo si la variable cantidadDivisores
		 *  fuese mayor a 2. Por lo tanto se cumple la condicion del else y se ejecuta en consola la respuesta de "si lo es"*/
		
		lector.close();
	}

}
