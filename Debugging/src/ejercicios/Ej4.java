package ejercicios;

import java.util.Scanner;

public class Ej4 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		int numeroLeido;
		int resultadoDivision;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un numero");
		numeroLeido = lector.nextInt();

		for(int i = numeroLeido; i > 0 ; i--){
			resultadoDivision = numeroLeido / i;
			System.out.println("el resultado de la division es: " + resultadoDivision);
		}
		
		/*El error se encuentra en la linea 20 en la condicion del bucle for --> i>=0. Esta condicion recoge que la i va a llegar hasta 0.
		 * En el momento que la i vale 0 y realiza la division, salta la excepcion porque no se puede dividir ningun numero para 0 porque no existe.
		 * Solucion: quitar el signo igual de la condicion del bucle for */
		lector.close();
	}

}
