package ejercicios;

import java.util.Scanner;

public class Ej3 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner lector;
		char caracter=105;
		int posicionCaracter;
		String cadenaLeida, cadenaFinal;
		
		lector = new Scanner(System.in);
		System.out.println("Introduce un cadena");
		cadenaLeida = lector.nextLine();
		/*El error se encuentra en la linea 13 del codigo porque se asigna a la variable char caracter el valor 27
		 * EL caracter 27 en el codigo ascii se corresponde con un espacio en blanco. Por esto, el substring est� fuera de rango
		 * Solucion: asignarle un valor a la variable char que se corresponda con un caracter en la tabla ascii*/
		posicionCaracter = cadenaLeida.indexOf(caracter);
		
		cadenaFinal = cadenaLeida.substring(0, posicionCaracter);
		
		System.out.println(cadenaFinal);
		
		
		lector.close();
	}

}
