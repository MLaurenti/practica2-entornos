package ejercicios;

import java.util.Scanner;

public class Ej2 {
	public static void main(String[] args){
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 */
		
		Scanner input ;
		int numeroLeido;
		String cadenaLeida;		
		
		input = new Scanner(System.in);
		
		System.out.println("Introduce un numero ");
		numeroLeido = input.nextInt();
		input.nextLine();
		/*Al leer un int y luego leer un string salta la excepcion, esto es debido a que hay que limpiar el buffer despues
		 *  de leer un int y antes de leer un string.
		 * El error esta en la linea 23 ya que es el paso intermedio entre la lectura del int y del string.
		 * EN la linea 33 tambien hay un error.El error salta si en la consola metes dos numeros diferentes, esto es debido a que no 
		 * se cumple la condicion del if. 
		 * SOlucion error1: limpiar el buffer entre la lectura del int y del string  --> input.nextLine();
		 * Solucion error2: detras del if poner un else para especificar el caso de que no se cumpla la condicion del if
		 */
		System.out.println("Introduce un numero como String");
		cadenaLeida = input.nextLine();
		
		if ( numeroLeido == Integer.parseInt(cadenaLeida)){
			System.out.println("Lo datos introducidos reprensentan el mismo número");
		}else {
			System.out.println("Los datos introducidos no representan el mismo numero");
		}
		
		input.close();
		
		
		
	}
}
