package ejercicios;

import java.util.Scanner;

public class Ej1 {

	public static void main(String[] args) {
		/*Establecer un breakpoint en la primera instrucción y avanzar
		 * instrucción a instrucción (step into) analizando el contenido de las variables
		 *  
		 */
		
		
		
		Scanner input;
		int cantidadVocales, cantidadCifras;
		String cadenaLeida;
		char caracter;
		
		cantidadVocales = 0;
		cantidadCifras = 0;
		
		System.out.println("Introduce una cadena de texto");
		input = new Scanner(System.in);
		cadenaLeida = input.nextLine();
		/*Al poner el igual detras del signo menor en la condicion del bucle for, te cuenta un car�cter mas
		 por lo que si pones una cadena de 4 caracteres, en la ultima reproduccion del bucle la i tomara el valor de 5
		 y saltara la excepcion.
		 SOLUCION: quitar el igual a la condicion del bucle
		 */
		for(int i = 0 ; i < cadenaLeida.length(); i++){
			caracter = cadenaLeida.charAt(i);
			if(caracter == 'a' || caracter == 'e' || caracter == 'i' 
					|| caracter == 'o' || caracter == 'u'){
					cantidadVocales++;
			}
			if(caracter >= '0' && caracter <='9'){
				cantidadCifras++;
			}
			
		}
		System.out.println(cantidadVocales + " y " + cantidadCifras);
		
		
		input.close();
	}

}
